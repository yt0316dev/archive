import 'dart:convert';

import 'package:charset/charset.dart';

String decodeCharCodes(List<int> codes) {
  try {
    return Utf8Decoder().convert(codes);
  } catch (_) {
    // fallthrough
  }

  try {
    return ShiftJISDecoder().convert(codes);
  } catch (_) {
    // fallthrough
  }

  return String.fromCharCodes(codes);
}
